defmodule Npuzzle.ServerTest do
  use ExUnit.Case

  alias Npuzzle.Server

  setup do
      Server.delete(:origin)
      Server.delete(:master)
      Server.delete(:size)
      Server.delete(:full_size)
  end

  describe "start_link/1" do
    test "Server starts automatically when starting the project" do
      assert {:error, {:already_started, _pid}} = Server.start_link(name: Npuzzle.Server)
    end
  end

  describe "update/2" do
    test "update/2 with valid parameter" do
      assert Server.update(:test, "am I a test") == :ok
    end
  end

  describe "get/0" do
    test "simple get/0 call" do
      Server.update(:test, "am I a test")
      assert Server.get() == %{test: "am I a test"}
    end
  end

  describe "get_by/1" do
    test "get_by/1 with valid argument" do
      Server.update(:test, "am I a test")
      assert Server.get_by(:test) == "am I a test"
    end

    test "get_by/1 with invalid argument" do
      Server.update(:test, "am I a test")
      assert Server.get_by(:flower) == nil
    end
  end

  describe "delete/1" do
    test "simple delete/1 call" do
      Server.update(:test, "am I a test")

      assert Server.get_by(:test) != nil
      assert Server.delete(:test) == :ok
      assert Server.get_by(:test) == nil
    end
  end
end
