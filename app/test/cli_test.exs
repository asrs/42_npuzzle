defmodule Npuzzle.CLITest do
  use ExUnit.Case
  import ExUnit.CaptureIO
  # doctest Npuzzle

  alias Npuzzle.CLI

  @usage """
  Usage: npuzzle [OPTION]... [FILE]

  """

  @help """
  Usage: npuzzle [OPTION]... [FILE]
  Solve the given npuzzle.
  Example: npuzzle ./my_puzzle
  Option:
  \t-v, --version\tshow the version
  \t-h, --help\tshow the help

  """

  @version """
  Npuzzle #{Mix.Project.config()[:version]}
  License GPlv3+: GNU GPL version 3 or later <http://www.gnu.org/licenses/licenses.html#GPL>
  This is free software: you are free to change and redistribute it.

  Written by Clement Richard and others, see <https://gitlab.com/mnhdrn>

  """

  describe "main/1 function" do
    test "main/1 with no args" do
      fun = fn ->
        assert CLI.main() == :ok
      end

      assert capture_io(fun) == @usage
    end

    test "main/1 with --help or -h" do
      fa = fn -> assert CLI.main(["--help"]) == :ok end
      fb = fn -> assert CLI.main(["-h"]) == :ok end

      assert capture_io(fa) == @help
      assert capture_io(fb) == @help
    end

    test "main/1 with --version or -v" do
      fa = fn -> assert CLI.main(["--version"]) == :ok end
      fb = fn -> assert CLI.main(["-v"]) == :ok end

      assert capture_io(fa) == @version
      assert capture_io(fb) == @version
    end

    test "main/1 with --help and --version" do
      fun = fn ->
        assert CLI.main(["-h", "-v"]) == :ok
      end

      assert capture_io(fun) == @help
    end

    test "main/1 with invalid args" do
      fun = fn ->
        assert CLI.main(["invalid_file"]) == :error
      end

      assert capture_io(fun) == "Invalid file format or inexistant file\n"
    end
  end
end
