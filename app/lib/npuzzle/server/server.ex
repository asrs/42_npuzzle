defmodule Npuzzle.Server do
  @moduledoc """
  This is a simpler server to maintain a state for all the data we
  will use in this software
  """
  use GenServer

  # API

  @spec start_link(any()) :: tuple()
  @doc """
  This function is the entrypoint of this module,
  it will init the server
  """

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @spec get :: tuple()
  @doc """
  This function will retrieve a Map with all the stocked data
  """

  def get do
    GenServer.call(__MODULE__, :get)
  end

  @spec get_by(atom()) :: tuple()
  @doc """
  This function will retrieve a data from a key in the server
  """

  def get_by(key) do
    GenServer.call(__MODULE__, {:get_by, key})
  end

  @spec update(atom(), any()) :: tuple()
  @doc """
  This function will update our data, at the given key with
  the given value
  """

  def update(key, value) do
    GenServer.cast(__MODULE__, {:update, key, value})
  end

  @spec delete(atom()) :: tuple()
  @doc """
  This function will delete from the server the given data.
  """

  def delete(key) do
    GenServer.cast(__MODULE__, {:delete, key})
  end

  # Callbacks

  def init(:ok) do
    {:ok, %{}}
  end

  def handle_cast({:update, key, value}, state) do
    new_state = Map.put(state, key, value)

    {:noreply, new_state}
  end

  def handle_cast({:delete, key}, state) do
    new_state = Map.delete(state, key)

    {:noreply, new_state}
  end

  def handle_call({:get_by, key}, _from, state) do
    new_state = Map.get(state, key)

    {:reply, new_state, state}
  end

  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end
end
