defmodule Npuzzle.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Npuzzle.Worker.start_link(arg)
      # {Npuzzle.Worker, arg}
      {Npuzzle.Server, name: Npuzzle.Server}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Npuzzle.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
