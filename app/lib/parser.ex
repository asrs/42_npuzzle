defmodule Npuzzle.Parser do
  @moduledoc """
  Npuzzle.Parser will read the given file path
  """

  alias Npuzzle.Server
  alias Npuzzle.Checker

  @err1 "No given file"
  @err2 "Invalid file format or inexistant file"

  @spec parse(tuple()) :: tuple()
  @doc """
  Parse/1 is the main function of our module.
  It will retrieve the first given file path.
  then give it to read/1.
  then the result is send to format/1
  and we return/1
  """

  def parse({:ok, []}), do: {:error, @err1}

  def parse({:halt, msg}), do: {:halt, msg}

  def parse({:error, msg}), do: {:error, msg}

  def parse({:ok, [head | _]}) do
    head
    |> File.read()
    |> format
    |> Checker.zero?
    |> return
  end

  def parse(_), do: {:error, @err1}

  @spec format(tuple()) :: tuple()
  @doc """
  The format/1 function will take the read file as a string.
  And format it to a list, will also stock in Npuzzle.Server during the
  transformation.
  """

  def format({:error, :enoent}), do: {:error, @err2}

  def format({:ok, file}) do
    file
    |> String.split("\n")
    |> Enum.drop(1)
    |> stock_size
    |> Enum.map(&String.replace(&1, ~R/[^\d\s]/, ""))
    |> Enum.flat_map(&String.split(&1, " ", trim: true))
    |> Enum.filter(&(&1 != []))
    |> Enum.map(&String.to_integer(&1))
    |> stock_data
  end

  def format(_), do: {:error, @err2}

  @spec stock_size(list()) :: list()
  ## this function will parse the size of given npuzzle
  # and put it on Npuzzle.Server GenServer

  defp stock_size(list) do
    n = List.first(list) |> String.to_integer()

    Server.update(:size, n)
    Server.update(:full_size, n * n)
    list |> Enum.drop(1)
  end

  @spec stock_data(list()) :: list()
  ## this function will store the unsorted and sorted list
  # in the cache of the application

  defp stock_data(list) do
    Server.update(:origin, list) &&
    Server.update(:master, snail_sort())
  end

  @spec snail_sort :: list
  ## This function will create a board in Spiral position
  # So we can check if the parsed board is solvable or not

  defp snail_sort do
    n = Server.get_by(:size)
    data = matrix(n, n, 1) |> Enum.concat
    max = Enum.max(data)

    data
    |> Enum.map(&(if &1 == max, do: 0, else: &1))
  end

  @spec matrix(integer, integer, integer) :: list
  ## this recursive function will generate element to add a row
  # in way to build our spiral sorted matrix.
  # it will add a row, an rotate until one of dimension = 0

  defp matrix(_, 0, _), do: [[]]

  defp matrix(a, b, c) do
    top = (c..(c + b - 1)) |> Enum.to_list
    bottom = matrix(b, a - 1, c + b) |> rotate

    [top | bottom]
  end

  @spec rotate(list) :: list
  ## this function will performe a clockwise rotation
  # on the matrix

  defp rotate(rows) do
    rows
    |> Enum.reverse
    |> Enum.zip
    |> Enum.map(&Tuple.to_list/1)
  end

  @spec return(tuple()) :: tuple()
  @doc """
  This function handle the final return of our function parse/1
  """

  def return(:ok), do: {:ok, "Parsing is done"}
  def return(_), do: {:error, @err2}
end
