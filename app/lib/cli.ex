defmodule Npuzzle.CLI do
  @moduledoc """
  Npuzzle.CLI is the entrypoint of our application.
  It contain the first function launch: main/1.

  This module will also parse the given arguments.
  """

  alias Npuzzle.Parser
  alias Npuzzle.Solver
  # alias heuristic

  @usage """
  Usage: npuzzle [OPTION]... [FILE]
  """

  @help """
  Usage: npuzzle [OPTION]... [FILE]
  Solve the given npuzzle.
  Example: npuzzle ./my_puzzle
  Option:
  \t-v, --version\tshow the version
  \t-h, --help\tshow the help
  """

  @version """
  Npuzzle #{Mix.Project.config()[:version]}
  License GPlv3+: GNU GPL version 3 or later <http://www.gnu.org/licenses/licenses.html#GPL>
  This is free software: you are free to change and redistribute it.

  Written by Clement Richard and others, see <https://gitlab.com/mnhdrn>
  """

  @spec main(list()) :: atom()
  @doc """
  This is the entrypoint function of our application.
  It will define the command line options, and parse the given
  arguments with OptionParser.parse/2.
  Then it will put the given argument to parse_opts/1
  before check the final return of the application with 
  private return/1
  """

  def main(args \\ []) when is_list(args) do
    options = [
      switches: [help: :boolean, version: :boolean],
      aliases: [h: :help, v: :version]
    ]

    OptionParser.parse(args, options)
    |> parse_opts
    |> Parser.parse
    |> Solver.solve
    |> return

    # IO.inspect Npuzzle.Server.get, limit: :infinity
  end

  @spec parse_opts(tuple()) :: tuple()
  @doc """
  This function take the parsed arguments and do some checking,
  it will return a tuple with one of the following atom: :ok, :halt, :error
  """

  def parse_opts({opts, file, _}) do
    cond do
      Keyword.get(opts, :help) -> {:halt, @help}
      Keyword.get(opts, :version) -> {:halt, @version}
      opts == [] and file == [] -> {:halt, @usage}
      true -> {:ok, file}
    end
  end

  @spec return(tuple()) :: atom()
  @doc """
  This function handle the final return of our application.
  In the case an :halt or an :error occured, it will print the message.
  """

  def return({:ok, _}), do: :ok

  def return({:halt, msg}), do: IO.puts(msg) && :ok

  def return({:error, msg}), do: IO.puts(msg) && :error
end
