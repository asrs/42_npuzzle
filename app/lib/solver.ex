defmodule Npuzzle.Solver do
  @moduledoc """
  Npuzzle.Solver
  """

  alias Npuzzle.Server
  alias Npuzzle.Checker

  @spec solve(tuple) :: tuple
  @doc """
  Entrypoint of the module this is here we gonna do
  the logic to verify a board, and then solve it
  """

  def solve({:error, msg}), do: {:error, msg}
  def solve({:halt, msg}), do: {:halt, msg}

  def solve({:ok, _}) do
    # IO.inspect Checker.size?
    # IO.inspect Checker.solvable?
    IO.inspect if Checker.solvable? == true, do: "SOLVABLE", else:  "UNSOLVABLE"
    {:ok, "yolo"}
  end
end
