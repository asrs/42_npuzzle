defmodule Npuzzle.Checker do
  @moduledoc """
  Npuzzle.Checker will give so tool to verify a board
  """

  alias Npuzzle.Server

  @spec zero?(atom) :: :ok | :error
  @doc """
  """

  def zero?(:ok) do
    origin = Server.get_by(:origin)

    case Enum.any?(origin, &(&1 == 0)) do
      true -> :ok
      _ -> :error
    end
  end

  def zero?(_), do: :error

  @spec size?() :: tuple()
  @doc """
  This function will take the number of case given in the file
  and check if the retrieve data is the same length.
  If not that's mean the file is unvalid.
  """

  def size? do
    origin = length(Server.get_by(:origin))
    master = length(Server.get_by(:master))
    size = Server.get_by(:full_size)

    case size > 2 and origin == size and master == size do
      true -> {:ok, "Board size is valid"}
      _ -> {:error, "Invalid board size"}
    end
  end

  @spec solvable?() :: boolean
  @doc """
  This function will check if a board is solvable or not.
  First we count inversion in :origin list.
  """

  def solvable? do
    n = Server.get_by(:size)
    origin = Server.get_by(:origin)
    master = Server.get_by(:master)

    ma_pos = Enum.find_index(master, &(&1 == 0))
    or_pos = Enum.find_index(origin, &(&1 == 0))
    ma_inv = inv_count(master, 0)
    or_inv = inv_count(origin, 0)

    if rem(n, 2) == 0 do
      rem(or_inv + or_pos, 2) == rem(ma_inv + ma_pos, 2)
    else
      rem(or_inv, 2) == rem(ma_inv, 2)
    end
  end

  @spec inv_count(list, integer) :: integer
  ## This function will count inversion in a list

  defp inv_count([], acc), do: acc

  defp inv_count([hd | td], acc) do
    inv_count(td, acc + inv_count_i(hd, td, 0))
  end

  @spec inv_count_i(integer, list, list) :: integer
  ## this function is just an utilitary for inv_count

  defp inv_count_i(_, [], acc), do: acc

  defp inv_count_i(i, [hd | td], acc) do
    if i != 0 and hd != 0 and i > hd do
      inv_count_i(i, td, acc + 1)
    else
      inv_count_i(i, td, acc)
    end
  end
end
