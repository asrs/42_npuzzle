# Npuzzle

The goal of this software is to solve the N-puzzle ("Taquin" in French) game using the A*
search algorithm or one of its variants.

## Installation

```
mix do deps.get, deps.compile
mix escript.build
```

## How to use


