FROM elixir:alpine

RUN apk --update add inotify-tools nodejs npm \
&& rm -rf /var/cache/apk/*

RUN mix do local.hex --force, local.rebar --force
RUN mix do archive.install hex phx_new --force

